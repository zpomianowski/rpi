#! /usr/bin/env venv/bin/python2
# -*- coding: utf-8 -*-
import glob
import logging
from json import loads, dumps
from time import sleep, time
from copy import deepcopy
from Queue import Queue, Full, Empty
from threading import Thread
from multiprocessing.pool import ThreadPool
from io import TextIOWrapper, BufferedRWPair
from enum import Enum
import serial
from serial.tools import list_ports_linux
from misc import threadsafe, Memoize
logger = logging.getLogger(__name__)


def get_duuid():
    from subprocess import check_output
    return check_output(
        'cat /var/lib/dbus/machine-id | md5sum',
        shell=True)[:-2].strip()


Q = Queue(maxsize=100)
HEARTBEAT = 1
DUUID = get_duuid()


def exe_control(function):
    def wrapper(that, msg=None):
        timeout = msg['timeout']

        period =msg['period']
        msg['start_time'] = time() + period
        msg['_executions'] += 1

        th = TaskThread(target=lambda: function(
            that, *msg['args'], **msg['vars']))
        th.start()
        logger.debug('[uuid:%(uuid)s] Running "%(task)s" task...' % msg)
        msg['_status'] = ExeStatus.RUNNING
        result = th.join(timeout=timeout)
        if result == ExeStatus.TIMEOUT:
            msg['_failures'] += 1
            logger.error('[uuid:%(uuid)s] Timeout!' % msg)
        msg['_status'] = ExeStatus.OK
        return (msg['uuid'], result)
    return wrapper


class ExeStatus(Enum):
    QUEUED = 0x1
    RUNNING = 0x2
    OK = 0x3
    FAILED = 0x4
    TIMEOUT = 0x5
    ABORTED = 0x6


class TaskThread(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}):
        Thread.__init__(self, group, target, name, args, kwargs)
        self.daemon = True
        self._return = ExeStatus.TIMEOUT

    def run(self):
        if self._Thread__target is not None:
            self._return = self._Thread__target(*self._Thread__args,
                                                **self._Thread__kwargs)
    def join(self, timeout):
        Thread.join(self, timeout)
        return self._return


class TaskExecutor(Thread):

    TMPLT = {
        'uuid': None,
        'duuid': DUUID,
        'group': None,
        'task': 'radio_report',
        'args': [],
        'vars': {},
        'start_time': None,
        'stop_time': None,
        'timeout': 15,
        'repeats': 1,
        'period': 0,
        'max_failures': 0,
        '_status': ExeStatus.QUEUED,
        '_executions': 0,
        '_failures': 0,
        '_result': None
    }
    UUDIS = []
    HIBERNATION = 1

    def __init__(self, client):
        super(TaskExecutor, self).__init__()
        self._modem = QuectelUSBInterface()
        self.__pool = ThreadPool(processes=4)
        self._client = client
        self._client.on_message = self.on_message
        self.daemon = True
        self.start()

    def run(self):
        while True:
            sleep(1)
            try:
                logger.debug('Active tasks: %s' % TaskExecutor.UUDIS)
                job_desc = Q.get_nowait()
            except Empty:
                if len(TaskExecutor.UUDIS) == 0:
                    sleep(TaskExecutor.HIBERNATION)
                continue
            now = time()

            uuid = job_desc['uuid']
            start_time = job_desc['start_time']
            stop_time = job_desc['stop_time']
            repeats = job_desc['repeats']
            executions = job_desc['_executions']
            status = job_desc['_status']

            if repeats == -1 and job_desc['period'] <= 0 and not stop_time:
                TaskExecutor.UUDIS.remove(uuid)
                logger.error('Invalid task configuration!')
                continue

            if (stop_time > now) or \
                    (executions >= repeats and repeats > 0) or \
                    status in [ExeStatus.FAILED, ExeStatus.ABORTED] or \
                    (job_desc['_failures'] >= job_desc['max_failures'] and
                     job_desc['max_failures'] != 0) or \
                    uuid not in TaskExecutor.UUDIS:
                try:
                    TaskExecutor.UUDIS.remove(uuid)
                except ValueError:
                    pass
                finally:
                    sleep(HEARTBEAT)
                continue

            if start_time > now or status == ExeStatus.RUNNING:
                logger.debug('[uuid:%s] Task postponed' % job_desc['uuid'])
                sleep(HEARTBEAT)
            else:
                self._resolve_task(job_desc)
            Q.put(job_desc)

    def exe_callback(self, return_value):
        payload = dict(
            uuid=return_value[0], duuid=DUUID,
            result=return_value[1])
        logger.info(dumps(payload, indent=2, sort_keys=True))
        # TODO


    def _resolve_task(self, job_desc):
        task_name = job_desc['task']

        func = None
        if task_name == 'radio_report':
            func = self.task_radio_report

        elif task_name in ['restart', 'reboot']:
            func = self.task_restart

        elif task_name == 'reconf':
            func = self.task_reconf

        elif task_name == 'active_tasks':
            func = self.task_active_tasks

        if func:
            self.__pool.apply_async(
                func, args=(job_desc,), callback=self.exe_callback)

    def on_message(self, msg):
        try:
            msg = loads(msg)
        except:
            logger.error('Could not load json!')
            return

        data = deepcopy(TaskExecutor.TMPLT)
        data.update(msg)

        if data['duuid'] != DUUID and data['duuid'] is not None:
            logger.debug('[uuid:%s] Task ignored (duuid does not match)')
            return

        if 'abort' in msg:
            if msg['abort'] in TaskExecutor.UUDIS:
                TaskExecutor.UUDIS.remove(msg['abort'])
                logger.warn('Task aborted')
            return
        try:
            if msg['uuid'] not in TaskExecutor.UUDIS:
                TaskExecutor.UUDIS.append(msg['uuid'])
                Q.put(data)
        except Full:
            logger.error('Task executor queue is full!')

    @exe_control
    def task_radio_report(self):
        return self._modem.get_info()

    def task_restart(self, msg=None):
        import os
        os.system('reboot')

    def task_reconf(self, msg=None):
        raise NotImplementedError()

    def task_active_tasks(self, msg=None):
        return { 'uuids': TaskExecutor.UUDIS }


# /etc/udev/rules.d/99-local.rules
# ACTION=="add", ATTRS{idVendor}=="2c7c", ATTRS{idProduct}=="0296",
# RUN+="/bin/sh -c 'echo 2c7c 0296 > /sys/bus/usb-serial/drivers/generic/new_id'"
class QuectelUSBInterface(object):
    VID = 0x2c7c
    PID = 0x0296

    def __init__(self):
        quectel_ports = []
        for s in list_ports_linux.comports():
            if QuectelUSBInterface.VID == s.vid and \
                    QuectelUSBInterface.PID == s.pid:
                quectel_ports.append(s)

        if len(quectel_ports) == 0:
            raise Exception(
                'No Qualcomm ports found! '
                'Check if VID:PID entry is added for USB driver!')

        ser = serial.Serial(timeout=1)
        ser.port = quectel_ports[len(quectel_ports)//2 - 1].device
        ser.baudrate = 115200
        self._s = ser

        self.query('ate0')
        self.query('at+creg=2')

        self.__info = dict(imei=None, imsi=None)

        self.__parse_ati(self.query('ati'))
        self.__parse_imsi(self.query('at+cimi'))
        self.__parse_imei(self.query('at+gsn'))

    def __parse_imei(self, data):
        if data[-1] == 'OK':
            self.__info['imei'] = data[0]

    def __parse_imsi(self, data):
        if data[-1] == 'OK':
            self.__info['imsi'] = data[0]

    def __parse_ati(self, data):
        d = self.__info
        try:
            d['manufacturer'] = data[0]
            d['model'] = data[1]
            d['revision'] = data[2].split(':')[1].strip()
        except:
            raise Exception('Cannot get basic data from the modem!')

    def __parse_csq(self, data):
        if data[-1] == 'OK':
            rssi, ber = data[0].split(':')[1].strip().split(',')
            rssi, ber = int(rssi), int(ber)
            if rssi < 99:
                rssi = -113 + 2*rssi
            elif rssi == 99:
                rssi = None
            elif rssi > 99 and rssi <= 191:
                rssi = -216 + rssi
            else:
                rssi = None
            return dict(rssi=rssi, ber=ber)

    def __parse_creg(self, data):
        if data[-1] == 'OK':
            stat, lac, cellid, act  = data[0].split(':')[1].strip().split(',')
            stat, lac, cellid, act  = int(stat), lac, cellid, int(act)
            return dict(
                status=stat,
                status_desc={
                    0: 'not registered',
                    1: 'registered to home network',
                    2: 'not registered, trying to attach or seraching',
                    3: 'registration denied',
                    4: 'unknown',
                    5: 'registered, roaming'}[stat],
                location_area_code=lac,
                cellid=cellid,
                access_technology=act,
                access_technology_desc={
                    0: 'GSM',
                    8: 'LTE Cat.M1',
                    9: 'LTE Cat.NB1'}[act])

    @Memoize(timeout=3)
    @threadsafe
    def query(self, cmd):
        for _ in range(3):
            try:
                self._s.open()
                self._s.write(cmd + '\r')
                sleep(.1)
                data = [s.strip() for s in self._s.readlines()]
                return [d for d in data if d]
            except serial.SerialException:
                logger.error('Serial communication failure!')
            finally:
                if self._s.is_open:
                    self._s.close()

    def get_info(self):
        data = deepcopy(self.__info)
        data.update(self.__parse_csq(self.query('at+csq')))
        data.update(self.__parse_creg(self.query('at+creg?')))
        return data

    def release(self):
        if self._s.is_open:
            self._s.close()


if __name__ == '__main__':
    m = QuectelUSBInterface()
    print m._s
    while True:
        print m.query('ati')
        print m.query('at+cimi')

        # print m.query('at+csq')
        # print m.query('at+creg?')
        # print m.query('at+qnwinfo')
        # print m.query('at+cimi')
