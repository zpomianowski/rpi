#! /usr/bin/env venv/bin/python2
# -*- coding: utf-8 -*-
import abc
from influxdb import InfluxDBClient
from urlparse import urlparse
from json import dumps
import paho.mqtt.client as mqtt
import logging
logger = logging.getLogger(__name__)


def get_client(uri, cfg=None):
    # issue https://bugs.python.org/issue18140 ;(
    qmark = '%3F'
    hmark = '%23'

    def enc(s):
        return s.replace('#', hmark).replace('?', qmark)

    def dec(s):
        return s.replace(hmark, '#').replace(qmark, '?')

    uri = urlparse(enc(uri))
    ctype = dec(uri.scheme)
    host = dec(uri.hostname)
    port = uri.port
    username = dec(uri.username)
    password = dec(uri.password,)
    path = dec(uri.path.replace('/', '').replace('\\', ''))

    if ctype.lower() == 'mqtt':
        return EMQClient(host, port, username, password, path, cfg)
    elif  ctype.lower() == 'inlfux':
        return InfluxClient(host, port, username, password, path, cfg)
    else:
        raise NotImplementedError('No client for this method!')


class IClient(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, host, port, username, password, path, cfg):
        raise NotImplementedError()

    @abc.abstractmethod
    def query(self, cmd):
        raise NotImplementedError()

    @abc.abstractmethod
    def send(self, payload):
        raise NotImplementedError()

    @abc.abstractmethod
    def release(self):
        raise NotImplementedError()


class InfluxClient(IClient):
    def __init__(self, host, port, username, password, path, cfg):
        self._client = InfluxDBClient(
            host, port, username, password, path, timeout=50)

    def query(self, cmd):
        return self._client.query(cmd)

    def send(self, payload):
        return self._client.write_points(payload)

    def release(self):
        pass


class EMQClient(IClient):

    MAPPING = {
        'c': 'energy_sample',
        'sc': 'energy_sum',
        'tdiff': 'meas_interval'
    }

    SUB_TOPIC = 'iot/tasks'
    PUB_TOPIC = 'iot/measurements'

    def __init__(self, host, port, username, password, path, cfg):
        client = mqtt.Client()
        if cfg:
            client.tls_set(cfg.get('db', 'cert_path'))
        client.username_pw_set(username, password=password)
        client.on_publish = self._on_publish
        client.on_connect = self._on_connect
        self.on_message = []
        client.on_message = self._on_message
        client.connect(host, port, 60)
        client.loop_start()
        self._client = client

    def _on_connect(self, client, userdata, flags, rc):
        self._client.subscribe(EMQClient.SUB_TOPIC)

    # method to be overriden for cfg changes
    def _on_message(self, client, userdata, msg):
        if not isinstance(self.on_message, (list, tuple)):
            self.on_message = [self.on_message]

        for f in self.on_message:
            if callable(f):
                f(msg.payload)

    def _on_publish(self, client, userdata, mid):
        pass

    def __refactor(self, payload):
        from datetime import datetime
        for item in payload:
            item['time'] = int(
                (datetime.strptime(item['time'], "%Y-%m-%dT%H:%M:%S.%fZ") -
                datetime.utcfromtimestamp(0)
                ).total_seconds())
            fields = item['fields']
            for k in fields.keys():
                newkey = EMQClient.MAPPING.get(k, None)
                if not newkey:
                    fields.pop(k)
                else:
                    fields[newkey] = fields.pop(k)
        return payload

    def query(self, cmd):
        from datetime import datetime
        return [[dict(time=str(datetime.utcnow()))]]

    def send(self, payload, channel=None):
        channel = channel if channel else EMQClient.PUB_TOPIC
        if channel == EMQClient.PUB_TOPIC:
            if not isinstance(payload, list):
                payload = [payload]
            payload = self.__refactor(payload)
        for p in payload:
            self._client.publish(channel, dumps(p), qos=1)
            logger.debug('Published to EMQ:\n%s' % dumps(p, indent=4))

    def release(self):
        self._client.loop_stop()


if __name__ == '__main__':
    from time import sleep, time
    from main import CFG
    from sys import stdout

    c = get_client(CFG.get('db', 'sink'), cfg=CFG)

    while True:
        option = int(raw_input('Choose option [1, 2, 3]:'))

        if option == 1:
            test_msg = [{
                'abort': '12345678911'
            }]
        elif option == 2:
            test_msg = [{
                'uuid': '12345678911',
                'start_time': time()
            }]
        elif option == 3:
            test_msg = [{
                'uuid': '12345678911',
                'start_time': time(),
                'repeats': -1,
                'period': 3
            }]
        c.send(test_msg, channel=EMQClient.SUB_TOPIC)