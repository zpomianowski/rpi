#!/bin/bash

# Użycie:
# ./skrypcik.sh INPUT_FILE OUTPUT_DIR
# INPUT_FILE - plik wejściowy, parametr wymagany
# OUTPUT_DIR - folder wyjściowy, opcjonalny,
#              domyślnie na bazie nazwy pliku INPUT FILE (bez rozszerzenia)

# Niezbędne narzędzia: ffmpeg -y ,bento4 tools
INPUT_FILE=$1
OUTPUT_DIR=${2:-$(echo $1 | rev | cut -d"." -f2-  | rev)}
rm -rf $OUTPUT_DIR

# Definiujemy workflow do wygenerowania plików w 4 jakościach

# parametr GOP_LEN_IN_SECONDS implikujący długość chunków w sekundach
GOPS=5

# low
ffmpeg -y -i $INPUT_FILE \
    -vf yadif=0:0:0,scale=320:-1 \
    -c:v libx264 -preset:v ultrafast -profile:v baseline -b:v 400k \
    -c:a aac -b:a 32k -ar:a 8k \
    -force_key_frames "expr:gte(t,n_forced*$GOPS)" \
-f mp4 __1_out.mp4

# medium
ffmpeg -y -i $INPUT_FILE \
    -vf yadif=0:0:0,scale=640:-1 \
    -c:v libx264 -preset:v fast -profile:v main -b:v 800k \
    -c:a aac -b:a 64k -ar:a 44100 \
    -force_key_frames "expr:gte(t,n_forced*$GOPS)" \
-f mp4 __2_out.mp4

# lowhigh
ffmpeg -y -i $INPUT_FILE \
    -vf yadif=0:0:0,scale=1280:-1 \
    -c:v libx264 -preset:v fast -profile:v main -b:v 2000k \
    -c:a aac -b:a 128k -ar:a 44100 \
    -force_key_frames "expr:gte(t,n_forced*$GOPS)" \
-f mp4 __3_out.mp4

# high
ffmpeg -y -i $INPUT_FILE \
    -vf yadif=0:0:0,scale=1980:-1 \
    -c:v libx264 -preset:v ultrafast -profile:v main -b:v 3500k \
    -c:a aac -b:a 192k -ar:a 48k \
    -force_key_frames "expr:gte(t,n_forced*$GOPS)" \
-f mp4 __4_out.mp4

# iterujemy po stworzonych plikach i je "fragmentujemy" po iframes
ls | grep -P "^__.*.mp4" | while read fname;
do
    mp4fragment $fname "${fname[@]: 1:-4}"frag.mp4
done

# generujemy strukturę chunków wraz z manifestem/manifestami
mp4dash --use-segment-timeline \
    $(IFS=$'\s'; echo "$(ls | grep -P "^_.*frag.mp4")" ) -o "${OUTPUT_DIR}"_dash
mp4hls \
    $(IFS=$'\s'; echo "$(ls | grep -P "^_.*frag.mp4")" ) -o "${OUTPUT_DIR}"_hls
ls | grep -P '^_{1,2}.*.mp4' | xargs -d'\n' rm
