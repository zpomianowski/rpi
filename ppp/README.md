# Quectel IoT Modem
Instalujemy `ppp` by wygodnie konfigurować modem i ustanawiać przez niego połączazenie sieciowe:
~~~~
:::bash
sudo apt install ppp
~~~~

Teraz konfigurujemy modem. Potem musimy poprawić wpis, w którym porcie USB mamy nasz modem (**ttyUSBX**):

~~~~
:::bash
sudo cp quectel* /etc/ppp/peer
~~~~

Jak już to mamy, ustawiamy by nasz modem działał domyślnie za każdym razem:
~~~~
:::bash
sudo sed -i "$/\n\nauto iot\niface iot inet ppp\n\tprovider quectel/m" /etc/network/interfaces
~~~~

...czyli **dodajemy** konfigurację do pliku `/etc/network/interfaces`:
~~~~
:::bash
auto iot
iface iot inet ppp
    provider quectel
~~~~