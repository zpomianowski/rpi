# RPI - Raspberry PI jako serwerek streamujący VOD, HLS, MPEG-DASH, RTMP oraz Demo SmartM2M #

List kroków pozwalająca zainstalować na Raspberry dodatkowe komponenty, które mogą się przydać w LAB M5.

Projekt należy rozpatrywać jako dwa niezależne projekty. Dla łatwiejszego utrzymania wszystko zostało zebrane tutaj w jedno repozytorium, jeden obraz dysku.

Ów projekty to:
- miniplatforma streamująca HLS/DASH
- aplikacja demonstrująca potencjał IOT i M2M

Środowiska _dev_ nie są stawiane na RPI, a na lokalnej maszynie, na której pracujemy.
Co trzeba skompilować na RPI, kompilujemy na RPI (bo tak szbciej i łatwiej), ale dla każdej sekcji _dev_ powinna być też sekcja _prod_, która omawia jak _cosik_ wrzucić na RPI.

Sekcje _instalacja_ są najczęściej instrukcjami jak coś skompilować bezpośrednio na RPI. Idealnie byłoby kompilować i tworzyć paczki pod procek ARM, ale nie warto w tym przypadku aż tak się bawić.
...chociaż zmieniam zdanie! Crosskompilowana na ARM paczka ''deb'' byłaby jednak mocno wskazana :P

---
## Domyślna konfiguracja
~~~~
[x] RTMP/HLS/DASH/VOD server
[x] DASH test stream
[ ] HLS test stream
[ ] Demo app enabled
[ ] Grafana (z nginx) na RPI
[ ] InfluxDB RPI
~~~~

---
## Obraz kart SD ##
Możemy wziąć gotowca lub go "uszyć" dla siebie i stworzyć dla dalszego powielania na inne komputerki.

### Na skróty ###
Bierzemy obraz _*.img_ karty SD, nagrywamy i wsio. [Zbyszek](mailto:zpomianowski@cyfrowypolsat.pl) powinien coś mieć.
Możemy też dopasować obecnie pracujące komputerki i na bazie ich konfiguracji stworzyć ów obraz by móc go propagować na pozostałych urządzeniach: [więcej szczegółow tutaj - to jeden z moich lepszych artykułów w wiki :)](http://atms.lab.redefine.pl/atms/default/wiki/disk-image-create-write-shrink).

### Generujemy kartę SD z systemem Raspbian ###
Potrzebujemy systemu operacyjnego:

- ściągamy obraz [Raspbiana (desktop - jest GUI, lite - wersja bez okienek)](https://www.raspberrypi.org/downloads/raspbian/)
- dobrze jest mieć zabawkę do nagrywania obrazów, polecam multiplatformowy [Etcher](https://etcher.io/)
- po wypaleniu karty mamy dwie partycje, jedną z nich jest `boot`
    - jeśli chcemy od razu mieć odpalony serwer _ssh_ (bardzo przydatna sprawa) to na `boot` tworzymy pusty plik `ssh`
    - po restarcie z naszym RPI powinniśmy móc się połączyć zdalnie: `ssh pi@ip_raspberry`
    - gdy nie wiemy jakie IP ma nasze RPI, warto przeskanować sieć takim narzędziem: `sudo nmap -sP -n 10.17.199.0/24`
- po świeżym zalogowaniu warto np. powiększyć przestrzeń dyskową o pozostałą przestrzeń karty, można to zrobić z konsoli narzędziem: `sudo raspi-config`

Przykładowy wydruk z `nmap`:
~~~~
Nmap scan report for 10.17.199.158
Host is up (-0.098s latency).
MAC Address: B8:27:EB:59:9B:50 (Raspberry Pi Foundation)
~~~~

---
## Instalacja serwera streamującego ##
Tutaj zastosowano wersje: `1.12.0` dla nginx oraz `1.1.7.10` dla nginx-rtmp-module. W razie potrzeby albo upgrade'u należy te wersje stosownie zmienić.
~~~~
:::bash
sudo apt update
sudo apt-get install libav-tools git build-essential libpcre3 libpcre3-dev libssl-dev python-dev htop
cd /tmp
mkdir nginx && wget -qO- https://nginx.org/download/nginx-1.12.2.tar.gz | tar xzv --strip-components=1 -C nginx
git clone https://github.com/kaltura/nginx-vod-module.git && cd nginx-vod-module && git checkout tags/1.22 && cd ..
git clone https://github.com/sergey-dryabzhinsky/nginx-rtmp-module.git && cd nginx-rtmp-module && git checkout 21db986 && cd ..
git clone https://github.com/arut/nginx-ts-module.git && cd nginx-ts-module && git checkout tags/v0.1.1 && cd ..
git clone https://github.com/arut/nginx-python-module.git && cd nginx-python-module && git checkout tags/v0.2.0 && cd ..
cd nginx
./configure --with-http_ssl_module --with-file-aio --with-cc-opt="-O0" --with-threads --with-debug \
    --add-module=../nginx-rtmp-module \
    --add-module=../nginx-vod-module \
    --add-module=../nginx-ts-module \
    --add-module=../nginx-python-module
make
sudo make install
~~~~
Nasz nginx powinien być zainstalowany i dostępny tutaj `/usr/local/nginx/sbin/nginx`. Teraz kopiujemy pliki konfiguracyjne:
~~~~
:::bash
cd ..
sudo cp systemd/*.service /lib/systemd/system
sudo cp nginx.conf /usr/local/nginx/conf/
~~~~
Aktywujemy serwisy:
~~~~
:::bash
sudo systemctl enable nginx-rtmp.service
sudo systemctl enable hls.service
sudo systemctl enable dash.service
~~~~
Musimy jeszcze utworzyć usera, który będzie robił nam za magazyn filmików:
~~~~
:::bash
sudo adduser vod
su vod
cd ~
mkdir video
cd video
wget http://atms-dev.lab.redefine.pl/vod/owca_720.flv
cp /tmp/rpi/dashplayer.html ..
# tworzymy ram dysk
mkdir tmp
su pi
sudo nano /etc/fstab
# dodajemy: tmpfs /home/vod/tmp tmpfs defaults,noatime,size=100M 0 0
~~~~
Rebootujemy maszynę! Można by było bez... ale wtedy więcej komend pośrednich do wklepania jest by odświeżyć działanie pewnych komponentów. Reboot jest na pewno szybszy dla nowicjusza :)
~~~~
sudo reboot
~~~~

---
## SmartM2M demo
Całość bazuje na kilku środowiskach / językach / technologiach / whatever.
Poniżej słowa klucze:

- influxdb, sqlite
- Ruby, RubyGems
- golang
- javascript, node, npm
- sass
- typescript, angular
- python, pip
- systemd

Ogólna koncepcja (na ten moment tylko grafana):
![Koncepcja](misc/concept.jpg "Koncepcja")

Testowane na:

- _Ubuntu 16.04 LTS_ - serwer
- _Raspbian Stretch Lite_ - koncentrator Raspberry PI

### Aplikacja DEMO
Trzy opcje:
```bash
./main.py               # odpala neverending loop
./main.py -h            # pomoc, krótkie how-to
./main.py --shell       # interaktywna powłoka, głównie do edycji db
./main.py --sim [X]     # symuluje na X dni wstecz, a potem odpala loop
                        # gdy X nie jest dane, pobierana jest wartość z pliku *.ini
./main.py --webdb       # odpala web interfejs do zarządzania bazą lokalną RPI
                        # serwis nie może być aktywny! sqlite jest jednowątkowy
./main.py --reset       # czysći bazę i historię pomiarów
                        # generuje domyślne rekordy wg ustawień w pliku *.ini
```

Tworzymy usera `m2m` dla naszego serwisu:
~~~~
:::bash
sudo useradd -r -s /bin/false m2m
~~~~
Instalujemy:
~~~~
:::bash
sudo ./push2rpi.py -h <ip_rpi> -u <user_rpi_sudoer>
~~~~

**Logi**:
RPI loguje swoje działania do plików, które są zdefiniowane w pliku `rpi/appconfig.ini` jako `logPath`. Logi są zamykane, gdy przekroczą wielkość 1MB i podlegają retencji - kolejne historyczne fragmenty zapisywane są z dodatkowymi numerami `*.log.X`. Im większy numer, tym starsze. Maksymalnie do **9**.

#### Środowisko dev
Bebechy dla RPI są tutaj:
~~~~
:::bash
cd m2m
sudo apt update
sudo apt install virtualenv python-dev
virtualenv venv
source venv/bin/activate
pip install --extra-index-url=https://gergely.imreh.net/wheels/ numpy
pip install -r requirements/dev.txt
cp appconfig.ini.example appconfig.ini
~~~~

Pracujemy i odpalamy sofcik w ramach izolowanego środowiska wirtualnego. Po aktywacji w okrągłych nawiasach powinna być widoczna nazwa: _(venv)_

---
## Inne ##
Dodatkowe funkcjonalności, które nie są domyślnie skonfigurowane. Mogą się przydać, nie muszą.

### Server DHCP - setup
**Do tego najlepiej podpiąć się do RPI lokalnie (monitorek HDMI i klawiatura USB), bez podpięcia do żadnej sieci LAN (unikamy syfu z racji na działanie dwóch serwerów DHCP jednocześnie)**.

##### Tryb serwera
Kilka kroków:
~~~~
:::bash
# Dezaktywnujemy klienta dhcp
sudo systemctl disable dhcpcd.service
sudo systemctl stop dhcpcd.service
# Kopiujemy plik ze statyczną konfiguracją sieci dla serwera
sudo cp /etc/network/interfaces.dhcpserver /etc/network/interfaces
# Aktywujemy serwer dnc/dhcp
sudo systemctl enable dnsmasq.service
sudo systemctl start dnsmasq.service
# Restartujemy usługi sieciowe
sudo systemctl restart networking.service
~~~~

##### Tryb klienta
Kilka kroków:
~~~~
:::bash
# Dezaktywujemy serwer dnc/dhcp
sudo systemctl disable dnsmasq.service
sudo systemctl stop dnsmasq.service
# Przywracamy domyślną konfiguracją sieci dla komputerka
sudo cp /etc/network/interfaces.default /etc/network/interfaces
# Aktywnujemy klienta dhcp
sudo systemctl enable dhcpcd.service
sudo systemctl start dhcpcd.service
# Restartujemy usługi sieciowe
sudo systemctl restart networking.service
~~~~

### USB modem ###
Gdy potrzebujemy innej komunikacji sieciowej niż WLAN i LAN.

#### Zmiana trybu USB ####
~~~~
:::bash
lsusb # Dla HW E398 12d1:1505
~~~~
Bierzemy _VendorID_ i _ProductID_ modemu (posłużymy się 12d1:1505). Potem...
~~~~
:::bash
sudo echo 'ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="12d1", ATTRS{idProduct}=="1505", RUN+="/usr/sbin/usb_modeswitch -v 12d1 -p 1505 -J"' | sudo tee /etc/udev/rules.d/98-huawei.rules
~~~~
Chodzi o to, że urządzenie USB może pracować jako mode, pendrive itp itd. Trzeba je odpowiednio _przełączyć_.
Co Vendor to inna historia. Magiczna opcja __-J__ jest dla Huaweia. Dla innych dostawców pewnie jest inaczej... trudniej / łatwiej?

### Stałe połączenie sieciowe via Modem (Quectel) ###
Opis jak to zrobić znajdziesz [tutaj](ppp/README.md).

### Dodatkowe informacje dla serwera streamującego ###
Możemy dodawać analogicznie nowe pliki i nowe serwisy na bazie _dash.service_.
Plikami wideo można zarządzac poprzez protokół sftp: `sftp://vod@<ip_naszego_rpi>`. Do tego celu można użyć narzędzia GUI np. _FileZilla_ / _WinSCP_ lub konsolowe _scp_.

[Dokumentacja dla nginx z modem streamującym rtmp](https://github.com/arut/nginx-rtmp-module/wiki/Directives)

[Dokumentacja systemd - kobyła do zarządzania serwisami w wielu distrosach linuksa](https://www.freedesktop.org/software/systemd/man/systemd.service.html)

### GRAFANA
To już jest klocek raczej stawiany na innej maszynie niż RPI. Niemniej tutaj opis jak postawić swoją, customizowaną wersję.

#### Instalacja InfluxDB i serwera reverse-proxy Nginx (Ubuntu)
Tutaj warto zaznaczyć. Grafana (tutaj na nginx) i InfluxDB nie muszą być na tej sej instancji serwera.
~~~~
:::bash
sudo apt update
sudo apt install nginx
wget https://dl.influxdata.com/influxdb/releases/influxdb_1.3.6_amd64.deb && sudo dpkg -i influxdb_1.3.6_amd64.deb && rm *.deb
wget https://dl.influxdata.com/chronograf/releases/chronograf_1.3.9.0_amd64.deb && sudo dpkg -i chronograf_1.3.9.0_amd64.deb && rm *.deb
# basepath do zmiany, jeśli chronograf ma być za reverse-proxy serwerem
sudo systemctrl start influxdb
~~~~
Dla innych platform niż _Debian_: [tutaj](https://portal.influxdata.com/downloads).

Konsola influxa, tworzymy bazę **m2m** dla projektu:
~~~~
:::bash
influx
> SHOW DATABASES
> CREATE DATABASE m2m
> SHOW DATABASES # powinniśmy zobaczyć nowy wpis "m2m"
> use m2m
> CREATE USER m2m WITH PASSWORD 'smartm2m' # przykład
> GRANT ALL ON m2m TO m2m
> SHOW GRANTS FOR m2m
~~~~

Plik konfiguracyjny `/etc/influxdb/influxdb.conf`

Bezpieczeństwo DB: **TODO**, na ten moment defaultowy user _root/root_

#### Środowisko dev - GRAFANA
Potrzebny jest upgrade **go** do **1.9.X**:
~~~~
:::bash
sudo add-apt-repository ppa:gophers/archive
sudo apt update
sudo apt-get install golang-1.9-go
~~~~

Sprawdzamy gdzie jest bin do **go** i przepinamy się ze starego środowiska na nowe:
~~~~
:::bash
sudo rm /usr/bin/go
sudo ln -s /usr/lib/go-1.9/bin/go go
~~~~

Będąc najwyżej w strukturze folderów tego projektu:
~~~~
:::bash
export GOPATH=`pwd`/grafana
cd grafana/src/github.com/grafana/grafana/
go run build.go setup
go run build.go build
sudo npm install -g yarn
yarn install --pure-lockfile
npm run build
~~~~

Podczas developerki przydaje się:
~~~~
:::bash
go get github.com/Unknwon/bra
bra run
# Problem: bra is not a recognized command
# Solution: export PATH=$PATH:$GOPATH/bin
~~~~

##### Build zoptymalizowanej paczki
~~~~
:::bash
sudo apt-get -y install ruby-all-dev
sudo gem install fpm
go run build.go build package
~~~~

##### HTTPS
Upewnij się, że SSL jest w wersji _DEV_.
~~~~
:::bash
sudo cp scripts/m2m /etc/nginx/sites-available/
cd /etc/nginx/sites-enabled/
sudo rm default
sudo ln -s ../sites-available/m2m m2m
sudo systemctl restart nginx.service
~~~~

### Środowisko prod - GRAFANA
Paczka wygenerowana w ramach dev (zoptymalizowana paczka).
~~~~
:::bash
sudo dpkg -i grafana_4.5.2-1509107521_amd64.deb
sudo systemctl start grafana-server
~~~~

#### Deployment i HTTPS - **TODO, bo brak publicznego serwera i domeny**
Upewnij się, że SSL jest w wersji _PROD_.
~~~~
:::bash
sudo cp scripts/m2m /etc/nginx/sites-available/
cd /etc/nginx/sites-enabled/
sudo rm default
sudo ln -s ../sites-available/m2m m2m
sudo systemctl restart nginx.service
~~~~

Zakładam generację certufikatów via letsenrcypt:
~~~~
:::bash
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get -y install python-certbot-nginx
~~~~

Jednorazowa konfiguracja:
~~~~
:::bash
sudo certbot --nginx
sudo certbot --nginx certonly # gdy ręcznie edytujemy congig nginx
~~~~
Certyfikaty letsencrypt mają ważność tylko 3 miesięcy, stąd potrzeba periodycznych aktualizacji - do crona roota trzeba dodać:
~~~~
:::bash
# m h dom mon dow   command
  0 5 10  *   *     certbot renew
~~~~
Taki wpis będzie przeprowadzał próbę przedłużenia ważności certyfikatu o 5:00 rano, 10. każdego miesiąca.